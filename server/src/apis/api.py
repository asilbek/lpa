from flask import Blueprint, request

from ..common.utils import make_json_response
from ..controller import user, customer

api = Blueprint('api', __name__)


@api.route('/', methods=['GET'])
def index():
    return make_json_response({})


@api.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    ret = customer.register(data)
    return make_json_response(ret)


@api.route('/login', methods=['POST'])
def login():
    data = request.get_json() or {}
    ret = user.login(data)
    return make_json_response(ret)
