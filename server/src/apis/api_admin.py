from flask import request, Blueprint, g

from ..common.error_codes import *
from ..common.exceptions import CoreException
from ..common.utils import make_json_response
from ..controller import user, customer

api_admin = Blueprint('api_admin', __name__)


@api_admin.before_request
def before_request():
    if request.method == 'OPTIONS':
        return
    if not hasattr(g, 'user') or not g.user:
        raise CoreException(USER_NOT_AUTHORIZED)


@api_admin.route('/', methods=['GET'])
def index():
    return make_json_response({'admin': True})


@api_admin.route('info', methods=['GET'])
def user_info():
    r = user.info()
    return make_json_response(r)


@api_admin.route('customer/list', methods=['GET'])
def get_users():
    data = customer.get_list()
    return make_json_response({"users": data})
