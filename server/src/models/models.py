import datetime
import json

from . import db, bcrypt
from .BaseModel import BaseModel
from ..common.Crypto import Crypt
from ..common.error_codes import GENERIC_ERROR
from ..common.exceptions import CoreException
from ..common.json_encoder import orm_to_json

SECRET_KEY = '1234564323456fgr'


class User(BaseModel):
    __tablename__ = 'user'

    name = db.Column(db.String(128), nullable=False)
    username = db.Column(db.String(128), nullable=False)
    password = db.Column(db.String(128), nullable=False)

    def __init__(self, data):
        super().__init__(data)
        self.password = self.__generate_hash(data.get('password'))

    def update(self, data):
        for key, value in data.items():
            if key == 'password':
                self.password = self.__generate_hash(data[key])
            else:
                try:
                    setattr(self, key, value)
                except:
                    pass
        self.update_date = datetime.datetime.utcnow()
        db.session.add(self)

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=10).decode("utf-8")

    def check_hash(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def to_json(self):
        user_json = orm_to_json(self)
        user_json.pop('password')
        return user_json


class Customer(BaseModel):
    __tablename__ = 'customer'

    def __init__(self, data):
        super().__init__(data)
        if not data.get('ssn'):
            raise CoreException(GENERIC_ERROR, u'enter SSN')

        self.ssn = Customer.encrypt_hash(data['ssn'])
        data.pop('ssn')

        if not data.get('first_name'):
            raise CoreException(GENERIC_ERROR, u'enter First Name')
        if not data.get('last_name'):
            raise CoreException(GENERIC_ERROR, u'enter Last Name')
        if not data.get('phone'):
            raise CoreException(GENERIC_ERROR, u'enter Phone')
        if not data.get('address'):
            raise CoreException(GENERIC_ERROR, u'enter Address')

        self.data = json.dumps(data)

    id = db.Column(db.Integer, primary_key=True)
    ssn = db.Column(db.String(128), nullable=False, unique=True)
    data = db.Column(db.String, nullable=False)

    @staticmethod
    def encrypt_hash(ssn):
        if not ssn or len(str(ssn)) < 5:
            raise CoreException(GENERIC_ERROR, u'ssn length error')

        crypt = Crypt()
        return crypt.encrypt(ssn, SECRET_KEY)

    def decrypt_hash(self):
        crypt = Crypt()
        return crypt.decrypt(self.ssn, SECRET_KEY)

    @staticmethod
    def get_by_ssn(value):
        hashed = Customer.encrypt_hash(value)
        return Customer.get_by_first(ssn=hashed)

    def to_json(self):
        user_json = orm_to_json(self)
        user_json['ssn'] = self.decrypt_hash()
        user_json.pop('data')
        data = json.loads(self.data)
        merged = {**user_json, **data}
        return merged
