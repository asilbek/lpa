from sqlalchemy.orm.exc import NoResultFound

from . import db
from ..common.error_codes import RECORD_NOT_FOUND
from ..common.exceptions import CoreException
from ..common.json_encoder import orm_to_json, json_to_orm


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    created_date = db.Column(db.DateTime, server_default=db.text('current_timestamp'), nullable=False)

    def __init__(self, data=None):
        if data:
            json_to_orm(data, self)

    def from_json(self, data):
        json_to_orm(data, self)

    @classmethod
    def get(cls, id):
        try:
            return db.session.query(cls).filter_by(id=id).one()
        except NoResultFound as e:
            raise CoreException(RECORD_NOT_FOUND)

    @classmethod
    def get_first(cls, id):
        return db.session.query(cls).filter_by(id=id).first()

    @classmethod
    def get_by(cls, **kw):
        return db.session.query(cls).filter_by(**kw).all()

    @classmethod
    def get_by_first(cls, **kw):
        return db.session.query(cls).filter_by(**kw).first()

    @classmethod
    def all(cls):
        return db.session.query(cls).all()

    @classmethod
    def count(cls):
        return db.session.query(cls).count()

    def save(self):
        db.session.add(self)

    def delete(self):
        db.session.delete(self)

    def to_json(self):
        return orm_to_json(self)
