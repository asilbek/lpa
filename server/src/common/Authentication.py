import datetime
import os
from functools import wraps

import jwt
from flask import g

from ..common.error_codes import GENERIC_ERROR, USER_NOT_AUTHORIZED
from ..common.exceptions import CoreException


class Auth():
    @staticmethod
    def generate_token(user_id):
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1),
                'iat': datetime.datetime.utcnow(),
                'sub': str(user_id)
            }
            return jwt.encode(payload, os.getenv('JWT_SECRET_KEY'), 'HS256').decode("utf-8")
        except Exception as e:
            raise CoreException(GENERIC_ERROR, 'error in generating user token')

    @staticmethod
    def decode_token(token):
        re = {'data': {}, 'error': ''}
        try:
            payload = jwt.decode(token, os.getenv('JWT_SECRET_KEY'))
            re['data'] = {'user_id': payload['sub']}
            return re
        except jwt.ExpiredSignatureError as e1:
            re['error'] = 'token expired, please login again'
            return re
        except jwt.InvalidTokenError:
            re['error'] = 'Invalid token, please try again with a new token'
            return re

    @staticmethod
    def auth_required(func):
        @wraps(func)
        def decorated_auth(*args, **kwargs):
            if not hasattr(g, 'user') or not g.user:
                raise CoreException(USER_NOT_AUTHORIZED)
            return func(*args, **kwargs)

        return decorated_auth
