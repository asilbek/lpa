import decimal
from datetime import datetime, date, time, timedelta

from flask import json
from sqlalchemy.ext.declarative import DeclarativeMeta


def orm_to_json(orm, exclude=None):
    if isinstance(orm, list):
        ret = []
        for o in orm:
            ret.append(orm_to_json(o, exclude))
        return ret
    else:
        d = {}
        for key in dir(orm):
            if key.startswith('_') or key == 'metadata' or key == 'query':
                continue
            value = getattr(orm, key)
            if hasattr(value, '__call__'):
                continue
            if exclude and key in exclude:
                continue
            if isinstance(value, datetime):
                value = value.strftime('%Y-%m-%d %H:%M:%S')
            d[key] = value
        return d


def json_to_orm(json_, orm):
    """
    Merge in items in the values dict into our object if it's one of our columns
    """
    if hasattr(orm, '__table__'):
        for c in orm.__table__.columns:
            if c.name in json_:
                setattr(orm, c.name, json_[c.name])
    else:
        for c in orm._asdict().keys():
            if c in json_:
                setattr(orm, c, json_[c])


class JSONEncoderCore(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            r = str(o)[:19]
            return r
        elif isinstance(o, date):
            return str(o)
        elif isinstance(o, time):
            r = str(o)
            return r
        elif isinstance(o, decimal.Decimal):
            return float(o)
        elif isinstance(o, timedelta):
            return o.total_seconds()
        elif isinstance(o.__class__, DeclarativeMeta):
            return orm_to_json(o)
        elif isinstance(o, bytes):
            return o.decode('utf8')
        else:
            return super(JSONEncoderCore, self).default(o)
