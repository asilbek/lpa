# coding=utf-8

from .error_codes import ERRORS


class CoreException(Exception):
    code = None

    def __init__(self, error_code, message=None, extra=None):
        self.code = error_code
        if message:
            self.message = message
        elif error_code in ERRORS:
            self.message = ERRORS[error_code]
        else:
            self.message = 'Unknown error with code {0}'.format(error_code)

    def __str__(self):
        return repr(self.code)
