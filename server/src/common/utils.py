# coding: utf-8

import decimal
import importlib
import json
import os
import re
from datetime import datetime, time, timedelta
from decimal import Decimal, getcontext
from hashlib import md5

import requests
from flask import Response

from . import error_codes
from .exceptions import CoreException
from .json_encoder import orm_to_json as _orm_to_json, json_to_orm as _json_to_orm, JSONEncoderCore as _encoder
from ..config import app_config

orm_to_json = _orm_to_json

json_to_orm = _json_to_orm

JSONEncoderCore = _encoder

GENERIC_DOMAINS = "aero", "asia", "biz", "cat", "com", "coop", \
                  "edu", "gov", "info", "int", "jobs", "mil", "mobi", "museum", \
                  "name", "net", "org", "pro", "tel", "travel"


def NoneToStr(s):
    return '' if s is None else s


def make_json_response(p_content=None):
    if p_content is None:
        p_content = {}
    if isinstance(p_content, list):
        p_content = {'list': p_content}
    if 'result' not in p_content:
        p_content.update({'result': 0})

    return Response(json.dumps(p_content, cls=JSONEncoderCore), mimetype='application/json; charset=utf-8')


def get_config_type(param='FLASK_ENV'):
    env_name = os.getenv(param)
    return app_config[env_name]


def get_config(param=None):
    if not param:
        return param
    configs = orm_to_json(get_config_type())
    return configs.get(param, '')


def is_dev():
    env_config = get_config_type()
    return env_config.DEBUG


def get_model(table_name):
    module = importlib.import_module('src.models')
    model = getattr(module, table_name)
    return model


def get_model_all(table_name):
    model = get_model(table_name)
    return model.all()


def get_model_by(table_name, id):
    model = get_model(table_name)
    return model.get(id)


def get_dir_value(table_name, id):
    dir_table = get_model_by(table_name, id)
    return dir_table.name if dir_table else ''
