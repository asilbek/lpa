from src.common.error_codes import *
from src.common.exceptions import CoreException
from src.models import Customer, db


def save(data):
    ssn = data.get('ssn')
    if not ssn:
        raise CoreException(GENERIC_ERROR, u'enter SSN')

    u = Customer.get_by_ssn(ssn)
    if u:
        CoreException(USER_ALREADY_EXISTS)
    u = Customer(data)
    u.save()
    db.session.flush()
    return u


def register(data):
    customer = save(data)
    user_json = customer.to_json()
    return {'customer': user_json}


def get_list():
    users = []
    for u in Customer.all():
        us = u.to_json()
        users.append(us)
    return users
