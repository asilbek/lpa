from flask import g

from src.common.Authentication import Auth
from src.common.error_codes import *
from src.common.exceptions import CoreException
from src.models import db, User


def save(data):
    username = data.get('username')

    if username:
        u = User(data)
        u.save()
        db.session.flush()
        return u
    else:
        CoreException(GENERIC_ERROR, 'username is mandatory')


def admin_create(data):
    user = save(data)
    user_json = user.to_json()
    token = Auth.generate_token(user.id)
    return {'user': user_json, 'token': token}


def login(data):
    user_in_db = None
    username = data['username']
    if username:
        user_in_db = User.get_by_first(username=username)

    if not user_in_db:
        raise CoreException(USER_NOT_FOUND)

    if not user_in_db.check_hash(data['password']):
        raise CoreException(WRONG_PASSWORD)

    token = Auth.generate_token(user_in_db.id)
    user_json = user_in_db.to_json()
    if user_json.get('password', False):
        user_json.pop('password')

    return {'user': user_json, 'token': token}


def info(user_id=None):
    the_user = g.user.to_json() if not user_id else User.get(user_id).to_json()
    if the_user.get('password', False):
        the_user.pop('password')

    if not the_user:
        raise CoreException(USER_NOT_FOUND)

    return {'user': the_user}
