import logging

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from src.app import create_app, db
from src.controller import user

app = create_app()

migrate = Migrate(app=app, db=db)

manager = Manager(app=app)
manager.add_command('db', MigrateCommand)


@manager.command
def init_db():
    print('init start')

    logging.info('generating roles')

    logging.info('generating admin user')
    user.save(
        {"username": "admin",
         "name": "admin",
         "password": "123123"}
    )


@manager.command
def create_db():
    db.create_all()


if __name__ == '__main__':
    manager.run()
