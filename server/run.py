import os

from dotenv import load_dotenv, find_dotenv
from flask_cors import CORS

from src.app import create_app

load_dotenv(find_dotenv())

app = create_app()

CORS(app, resources=r'*', supports_credentials=True,
     allow_headers=['Content-Type', 'api-token', 'Access-Control-Allow-Origin'])

if __name__ == '__main__':
    port = os.getenv('PORT')
    app.run(host='0.0.0.0', port=port)
