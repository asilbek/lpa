bcrypt==3.1.7
Flask==1.1.2
Flask-Bcrypt==0.7.1
Flask-Migrate==2.5.3
Flask-Script==2.0.6
Flask-SQLAlchemy==2.4.3
python-dotenv==0.13.0
requests==2.23.0
SQLAlchemy==1.3.17
Werkzeug==1.0.1
flask_cors==3.0.8
PyJWT==1.7.1
# simple-crypt==4.1.7
# cryptocode==0.1
pycrypto==2.6.1
