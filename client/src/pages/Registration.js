import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Input, {useInput} from "../components/Input";
import requester from "../common/requester";
import api from "../common/apis";
import Paper from "@material-ui/core/Paper";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import AppStore from "../storage/AppStore";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 0),
  },
}));


export default () => {
  const classes = useStyles();

  const firstInput = useInput('');
  const lastInput = useInput('');
  const phoneInput = useInput('');
  const ssnInput = useInput('');
  const addressInput = useInput('');

  const handleRegister = () => {
    api.register({
      ssn: ssnInput.value,
      first_name: firstInput.value,
      last_name: lastInput.value,
      phone: phoneInput.value,
      address: addressInput.value
    }).then(r => {
      AppStore.setAlert('SUCCESS', true);

      firstInput.onChange('');
      lastInput.onChange('');
      phoneInput.onChange('');
      ssnInput.onChange('');
      addressInput.onChange('');
    });
  }

  const disabled = !firstInput.value || !lastInput.value || !phoneInput.value || !ssnInput.value || !addressInput.value;

  return (
    <>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>

        <Card elevation={3}>
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <Input
                  autoComplete="fname"
                  name="firstName"
                  variant="outlined"
                  required
                  fullWidth
                  id="firstName"
                  label="First Name"
                  autoFocus
                  {...firstInput}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Input
                  required
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                  {...lastInput}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <Input
                  required
                  fullWidth
                  id="phone"
                  label="Telephone Number"
                  name="phone"
                  autoComplete="phone"
                  {...phoneInput}
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <Input
                  required
                  fullWidth
                  id="ssn"
                  label="SSN"
                  name="ssn"
                  autoComplete="ssn"
                  {...ssnInput}
                />
              </Grid>
              <Grid item xs={12}>
                <Input
                  required
                  fullWidth
                  name="address"
                  label="Full Address"
                  id="address"
                  autoComplete="address"
                  {...addressInput}
                />
              </Grid>
            </Grid>
            <Button
              disabled={disabled}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={handleRegister}
            >
              Sign Up
            </Button>
          </CardContent>
        </Card>
      </div>
    </>
  )
}
