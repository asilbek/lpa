import LocalStorage from "./LocalStorage";
import api from "../common/apis";
import {action, computed, observable} from 'mobx'

// let busy_counter = observable.box(0);

class AppStore {
  token = '';
  @observable level = '';
  @observable message = '';
  @observable user = null;
  @observable busyCounter = 0;
  @observable successMessage;

  @computed
  get isBusy() {
    return this.busyCounter > 0;
  }

  @action
  setBusy(incr) {
    if (!incr) {
      this.busyCounter = this.busyCounter > 0 ? this.busyCounter - 1 : 0;
    } else {
      this.busyCounter = this.busyCounter + 1;
    }
  }

  @action
  setToken(token) {
    this.token = token;
    if (token) {
      LocalStorage.save('token', token);
    } else {
      LocalStorage.remove('token');
    }
  }

  @action
  setUser(user) {
    this.user = user;
    if (user) {
      LocalStorage.save('user', user);
    } else {
      LocalStorage.remove('user');
    }
  }

  @action
  loadToken() {
    if (this.token) {
      return this.token;
    }
    const token = LocalStorage.get('token');
    this.token = token;
    return token;
  }

  @action
  async loadData() {
    await this.loadToken();
    await this.getUserInfo();
  }

  @action
  async getUserInfo() {
    if (this.token) {
      const r = await api.getUserInfo();
      this.user = r.user;
      return this.user;
    }
    return null;
  }

  @action
  logout() {
    this.token = null;
    this.user = null;
    LocalStorage.clear();
    this.clearAlert();
  }

  @action
  async login(username, password) {
    const response = await api.login(username, password);
    this.setToken(response.token);
    this.setUser(response.user);
    return response;
  }

  @action
  clearAlert() {
    this.message = '';
    this.successMessage = null;
  }

  @action
  setAlert(message, success) {
    this.message = message;
    this.successMessage = success;
  }
}

export default new AppStore();

