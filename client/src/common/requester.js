import React from "react";
import axios from "axios";
import LocalStorage from "../storage/LocalStorage";
import AppStore from "../storage/AppStore";

const BASE_URL = 'http://localhost:1111/api/';

axios.defaults.baseURL = BASE_URL;
axios.defaults.responseType = "json";
axios.defaults.timeout = 30000;

async function request(method, url, reqData, params) {
  AppStore.setBusy(true);
  reqData = reqData || {};
  let headers = {"Content-Type": "application/json", 'Access-Control-Allow-Origin': '*'};
  const token = AppStore.loadToken();
  if (token) {
    headers["api-token"] = token;
  }

  try {
    let {data} = await axios.request({method, url, data: reqData, params, headers});
    AppStore.setBusy(false);
    if (data.result === 0) {
      return data;
    }
    AppStore.setAlert(data.message);

    return Promise.reject(data);
  } catch (error) {
    AppStore.setBusy(false);
    console.warn(error);
    return Promise.reject(error);
  }
}

const requester = {
  get(url, params = null) {
    return request("get", url, null, params);
  },
  post(url, data = null,) {
    return request("post", url, data, null);
  },
};

export default requester;
