import requester from '../common/requester';


const api = {
  register(data) {
    return requester.post('register', data);
  },

  getUserInfo() {
    return requester.get('admin/info');
  },
  login(username, password) {
    return requester.post('login', {username, password});
  },
  getCustomers() {
    return requester.get('admin/customer/list');
  }
};

export default api;
