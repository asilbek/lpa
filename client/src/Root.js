import React, {useState} from 'react';
import Container from '@material-ui/core/Container';
import Header from "./components/Header";
import Footer from "./components/Footer";
import Registration from "./pages/Registration";

import {HashRouter as Router, Route, Switch, Redirect} from "react-router-dom";
import Admin from "./pages/Admin";
import AppStore from "./storage/AppStore";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import {makeStyles} from '@material-ui/core/styles';
import {Observer} from "mobx-react";

const ProtectedRoute = ({component: Component, ...rest}) => (
  <Route {...rest} render={(props) => (
    <Observer>{() => {
      if (AppStore.token && AppStore.token.length > 0) {
        return <Component {...props} />
      }
      return <Redirect to='/'/>
    }}
    </Observer>
  )}/>
)

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const Root = () => {

  const classes = useStyles();

  React.useEffect(() => {
    AppStore.loadData();
  }, [])

  const closeMessage = () => {
    AppStore.setAlert('')
  }

  return (
    <Observer>{() => (
      <Router>
        <Header/>
        <Container maxWidth="md">
          <Switch>
            <Route exact path="/">
              <Registration/>
            </Route>
            <ProtectedRoute path="/admin" component={Admin}/>
          </Switch>
        </Container>
        <Footer/>
        <Snackbar open={!!AppStore.message}
                  autoHideDuration={6000}
                  onClose={closeMessage}
                  anchorOrigin={{vertical: 'top', horizontal: 'center'}}
        >
          <Alert onClose={closeMessage} severity={AppStore.successMessage ? "success" : "error"}>
            {AppStore.message}
          </Alert>
        </Snackbar>
        <Backdrop className={classes.backdrop} open={AppStore.isBusy}>
          <CircularProgress color="inherit"/>
        </Backdrop>
      </Router>)}
    </Observer>
  )
}
export default Root;
