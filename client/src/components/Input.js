import TextField from "@material-ui/core/TextField";
import React, {useState} from "react";

export function useInput(initialValue) {
  const [value, setValue] = useState(initialValue);
  const onChange = text => setValue(text);
  return {value, onChange}
}

const Input = ({onChange, ...rest}) => (
  <TextField variant="outlined"
             onChange={e => {
               if (onChange) {
                 onChange(e.target.value);
               }
             }}
             {...rest}
  />
)

export default Input

