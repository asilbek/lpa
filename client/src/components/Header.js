import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from "@material-ui/core/Dialog";
import Input, {useInput} from "./Input";
import {NavLink, useHistory} from "react-router-dom";
import store from "../storage/AppStore";
import AppStore from "../storage/AppStore";
import {Observer} from "mobx-react";

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'flex',
    flex: 1,
    flexDirection: 'row'
  },
  menu: {
    marginLeft: 20,
    color: 'white'
  },
  headerButton: {
    float: 'right'
  }
}));

const Header = () => {
  const classes = useStyles();
  const history = useHistory()

  // const {user, token, setUser, setToken, logout} = React.useContext(AppContext);
  const usernameInput = useInput('');
  const passwordInput = useInput('');

  const [open, setOpened] = React.useState(false);

  const handleOpen = () => {
    setOpened(true);
  }

  const handleClose = () => {
    setOpened(false);
  }

  const handleLogin = () => {
    AppStore.login(usernameInput.value, passwordInput.value)
      .then(() => {
        history.push('/admin');
        handleClose();
      })
  }

  const handleLogout = () => {
    store.logout();
    history.push('/');
  }

  return (
    <Observer>{() => (
      <>
        <AppBar position="static">
          <Toolbar>
            <Avatar alt="Remy Sharp" src="https://cdn.logo.com/hotlink-ok/logo-social.png"/>
            <NavLink to="/">
              <Typography variant="h6" className={classes.menu}>
                Registration
              </Typography>
            </NavLink>
            {!!store.user &&
            <NavLink to="/admin">
              <Typography variant="h6" className={classes.menu}>
                Customers
              </Typography>
            </NavLink>
            }
            <div style={{flex: 1}}/>
            {!store.user &&
            <Button color="inherit" className={classes.headerButton} onClick={handleOpen}>Login</Button>}
            {!!store.user && <>
              <Avatar alt={store.user.username} src={store.user.username}/>
              <Button color="inherit" onClick={handleLogout}>Logout</Button>
            </>}
          </Toolbar>
        </AppBar>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Login</DialogTitle>
          <DialogContent>
            <DialogContentText>
              This is login Form
            </DialogContentText>

            <Input
              autoFocus
              id="username"
              label="User Name"
              fullWidth
              {...usernameInput}
            />

            <Input
              style={{marginTop: 20}}
              id="password"
              label="Password"
              type="password"
              fullWidth
              {...passwordInput}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color='secondary'>
              Cancel
            </Button>
            <Button onClick={handleLogin}
                    color="primary"
                    variant="contained"
                    disabled={!usernameInput.value || !passwordInput.value}
            >
              Login
            </Button>
          </DialogActions>
        </Dialog>
      </>)}
    </Observer>
  )
}

export default Header;
