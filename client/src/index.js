import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './App.css';
import {ThemeProvider} from '@material-ui/core/styles';
import {Provider as MobxProvider} from 'mobx-react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Root from "./Root";
import theme from "./common/theme";
import appStore from "./storage/AppStore";

const stores = {appStore}

ReactDOM.render(
  <MobxProvider {...stores}>
    <ThemeProvider theme={theme}>
      <CssBaseline/>
      <Root/>
    </ThemeProvider>
  </MobxProvider>,
  document.getElementById('root')
);

